import React from 'react';
import './Footer.css'


class Footer extends React.Component {
  // constructor(props) {
  //   super(props);
  // }
   
    render() {
      
  
    return (
      <div className = "footer">
        <div>
          <p>Kiev 2021</p>
          <p>All Rights Reserved</p>
          </div>
          <div className = "hideMob">
            <ul>
              <li><button>Карта сайта</button></li>
              <li><button>Google Sitemap</button></li>
            </ul>
          </div>
          <div className = "hideMob">
            <ul>
              <li><a href="/contacts">Контакты</a></li>
              <li><a href = "/about">О Сервисе</a></li>
              <li><a href = "/servise">Соглашение о использовании сервиса</a></li>
            </ul>
          </div>
          </div>
    );
    }
  }
  
  
  
  export default Footer;