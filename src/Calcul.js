import React from 'react';
import './Calcul.css'


class Calcul extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        val : '',
        result : '0'
      }

    }
    submitRate = (e) => {
        e.preventDefault();
        let elements = e.target.elements;
        let countCurency = elements['count-curency'].value;
        let typeCurency = elements['type-curency'].value;
        this.setState({result : (countCurency / this.state.rate[typeCurency]).toFixed(3)});

    }
    clean= () =>{
      <input type='number' name = 'count-curency' defaultValue=''/>
    }
    static getDerivedStateFromProps(props, state) {
      return {rate : props.rate};
}
    render() {
      
  
        return (
            <div className='homeCalc'>
            <p style = {{textAlign : "start", fontSize : "20px", fontWeight : "500", color : "rgb(116, 107, 107)"}}>Калькулятор обмена</p>
            <div className = "calcul">
            <p style = {{paddingBottom : "10px", textAlign : "start", fontSize : "15px", fontWeight : "500", color : "rgb(116, 107, 107)"}}>Я хочу поменять</p>
              <div className = "valute" style = {{paddingTop : "10px"}}>
                  <form onSubmit = {this.submitRate} className = "valute">
              <input onChange = {this.change} type='number' name = 'count-curency' defaultValue='' list='100' style={{  color: "rgb(116, 107, 107)", border: "1px solid rgb(187, 187, 187)"}}></input><br/>
              <select name = 'type-curency'style={{  color: "rgb(116, 107, 107)", backgroundColor: "rgb(232, 231, 231)", border: "1px solid rgb(187, 187, 187)"}} >
                
            {Object.keys(this.props.rate).map((keyName, i) => (
                  <option key = {keyName} value = {keyName}>{keyName}</option>
                )
                )}
          </select>
          <input type="submit" defaultValue = "calc" style={{color: "rgb(116, 107, 107)", backgroundColor: "rgb(232, 231, 231)", border: "1px solid rgb(187, 187, 187)"}}/>
          </form>
          </div>
          <p style = {{textAlign : "start", fontSize : "15px", fontWeight : "500", color : "rgb(116, 107, 107)", paddingTop : "10px"}}>Результат</p><br/>
          <p style = {{textAlign : "start", fontSize : "15px", fontWeight : "500", color : "rgb(116, 107, 107)"}}>EUR {this.state.result}</p><br/>

          </div>
          </div>

        );
    }
}
export default Calcul;