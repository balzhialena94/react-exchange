import React from 'react';
import './Contacts.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';



class Contacts extends React.Component {
    
   
    
   
    render() {

  
    return (
      <div className = "contacts">
         <h3>Контактные данные</h3>
         <div className = "contacts-box">
           <div>
           <FontAwesomeIcon icon={faMobileAlt} className="icons"/><br/>
           <b>Позвоните нам</b>
           <p>Есть вопросы? Мы поможем!</p>
           <a href="tel:+380731111111">+38(073)-111-11-11</a><br/>
           <a href="tel:+380671111111">+38(067)-111-11-11</a><br/>
           <a href="tel:+380951111111">+38(095)-111-11-11</a><br/>
           </div>
           <div>
           <FontAwesomeIcon icon={faEnvelope} className="icons"/><br/>
           <b>Напишите нам</b>
           <p>Идеи? Предложения?</p>
           <p>Мы открыты для любых вопросов</p>
           <p>Написать на <a href="mailto:balzhialena94@gmail.com?subject=Вопрос по React">react.exchange</a></p>
           </div>
         </div>
        </div>
    );
    }
  }
  
  
  
  export default Contacts;