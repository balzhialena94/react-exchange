import React from 'react';
import './Header.css'

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      home : 'Главная',
      about : 'Пункты обмена',
      contacts : 'Контакты'
    }
    
  }  
    render() {
      
  
    return (
      <div className = "header">
        <p>React exchange</p>
          <nav>
            <ul className = "navigation">
            <li><a href="/">{this.state.home}</a></li>
            <li><a href="/about">{this.state.about}</a></li>
            <li><a href="/contacts">{this.state.contacts}</a></li>
            </ul>
          </nav>
      </div>
    );
    }
  }
  
  
  
  export default Header;