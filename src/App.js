import './App.css';
import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contacts from './Contacts';
import Header from './Header';
import Footer from './Footer';
import Servise from './Servise';




class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
    
  }

  render() {
    

  return (
    <Router>
          <Header/>
    <div className="App"> 
    <Switch>
      <Route exact path="/" component = {Home}></Route>
      <Route exact path="/about" component = {About}></Route>
      <Route exact path="/contacts" component = {Contacts}></Route>
      <Route exact path="/servise" component = {Servise}></Route>
    </Switch>
    </div>
    <Footer/>    
    </Router>

  )
  }
}



export default App;
