import React from 'react';
import Calcul from './Calcul';
import './Course.css'

class Course extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        date : '',
        currencyRate : {}, 
      }
      this.currency = ['USD' , 'RUB' , 'UAH', 'CAD'];
    }

     componentDidMount() {
       this.getRate();
     }

      getRate = () => {
        fetch('http://api.exchangeratesapi.io/v1/latest?access_key=6ac42f60f872adab6e7b8bd5165f1e04')
        .then(data => data.json())
        .then(data => {
          const result = {};
          for(let i = 0 ; i < this.currency.length; i++){
            result[this.currency[i]] = data.rates[this.currency[i]];
          }
          this.setState ({
            currencyRate : result,
             date : data.date
            });
        })
      }
  
      render() {
       
    
      return (
        <div className = 'box'>
              <p style = {{textAlign : "start", fontSize : "20px", fontWeight : "500", color : "rgb(116, 107, 107)"}}>Курс валют на {this.state.date}</p>
              <div className = "courseBox">
                {Object.keys(this.state.currencyRate).map((keyName, i) => (
                  <div className = 'blockRate' key = {keyName}>
                 <div className='nameVal'> {keyName}</div><br></br>
                 <div>{this.state.currencyRate[keyName].toFixed(2)}*</div><br/>
                 <div className='textVal'>
                   <p>* Можно купить за 1 EUR</p></div>
                 </div>
                )
                )}
                </div>
                <Calcul rate = {this.state.currencyRate}></Calcul>
                </div>
      
      );
      }

    }
    export default Course;